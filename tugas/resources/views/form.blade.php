<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Form Sign Up</title>
</head>

<body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <br>
        <form method="POST" action="/welcome">
                @csrf

                <label for="fname">First name:</label><br>
                <input type="text" id="fname" name="fname"><br>
                <label for="lname">Last name:</label><br>
                <input type="text" id="lname" name="lname"><br>

                <p>Gender:</p>
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label><br>

                <p>Nationaly:</p>
                <select id="country" name="country">
                        <option value="Indonesia">Indonesia</option>
                        <option value="Amerika">Amerika</option>
                        <option value="Inggris">Inggris</option>
                </select>

                <p>Language Spoken</p>
                <input type="checkbox" id="bahasa" name="bahasa" value="bahasa">
                <label for="bahasa"> Bahasa Indonesia</label><br>
                <input type="checkbox" id="english" name="english" value="english">
                <label for="english"> English</label><br>
                <input type="checkbox" id="other" name="other" value="other">
                <label for="other"> Other</label><br>

                <p>Bio:</p>
                <textarea name="message" rows="10" cols="30"></textarea><br>
                <input type="submit" value="kirim">
        </form>
</body>

</html>